module.exports = {
    apps: [{
        name: "cicd",
        script: "./index.js",
        instance_var: 'INSTANCE_ID',
        instances: 'max',
        env: {
            NODE_ENV: "development"
        },
        env_production: {
            NODE_ENV: "production"
        },
        log_file: 'log_file.log'
    }]
}